const express = require("express");

// Create an application using express
const app = express();

const port = 3000;

// Allows your app to read JSON data
app.use(express.json());

// Allows your app to read data in any forms
app.use(express.urlencoded({extended:true}));


// [SECTION] Routes
// Express has methods corresponding to each HTTP method

// GET
// This route expects to receive a "GET" at the "/greet" endpoint
app.get("/greet", (request, response) => {
	// "respond.send" uses the express.js module's method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
});


// POST
app.post("/hello", (request, response)=> {
	// "request.body" contains the content/data of the request body
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
});


let users = [];

app.post("/signup", (request, response) => {
	console.log(request.body);
	if(request.body.username !== '' && request.body.password !== ''){

		// This will store the user object sent via Postman to the array created above
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input BOTH username and password.")
	}
});


// PATCH
app.patch("/change-password", (request, response) => {

	let message;

	// Creates a for loop that will loop through the elements of the "users" array 
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		if(request.body.username == users[i].username){
			// Changes the password of the user found by the loop into the passowrd provided in the client/Postman.
			users[i].password = request.body.password;

			message = `User ${request.body.username}'s password has been updated.`
			break;
		} else {
			message = "User does not exist"
		}
	}
	response.send(message);

});



////////////////////////////////////////////////////////////
// [SECTION]  Activity

app.get("/home", (request, response) => {
	response.send("Welcome to homepage")
});


let users1 = [{
	"username": "johndoe",
	"password": "johndoe1234"
	},
	{
	"username": "janesmith",
	"password": "janesmith1234"
	}
]

app.get("/users", (request, response) => {
	response.send(users1)
});



app.delete("/delete-user", (request, response) => {
	console.log(request.body)
	for(let i = 0; i < users1.length; i++){
		
		if(request.body.username == users1[i].username){
			 users1.splice(i, 1)
		response.send(`User ${request.body.username} has been deleted`)
			break;
		} else {
			response.send("User does not exist")
		}
	}
	
});






app.listen(port, ()=> console.log(`Server running at port ${port}`));